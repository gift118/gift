import { Injectable, UsePipes } from '@nestjs/common'
import {
  accountGetUserProfileKey,
  AccountGetUserProfileRequest,
  AccountGetUserProfileResponse,
  accountUpdateUserProfileKey,
  AccountUpdateUserProfileRequest,
  AccountUpdateUserProfileResponse,
  ResponseStatuses,
} from '@gift/contracts'
import { RabbitPayload, RabbitRPC } from '@golevelup/nestjs-rabbitmq'
import { getDefaultPipeValidator, replyErrorHandler } from '@gift/common'
import { UserService } from './user.service'

@Injectable()
export class UserController {
  constructor(private readonly userService: UserService) {}

  @UsePipes(getDefaultPipeValidator())
  @RabbitRPC({
    routingKey: accountGetUserProfileKey,
    queue: accountGetUserProfileKey,
    exchange: process.env.AMQP_EXCHANGE,
    errorHandler: replyErrorHandler,
  })
  async getUserProfile(
    @RabbitPayload() payloadReq: AccountGetUserProfileRequest,
  ): Promise<AccountGetUserProfileResponse> {
    const payload = await this.userService.getUserProfileById(payloadReq.userId)
    return {
      payload,
      status: ResponseStatuses.success,
    }
  }

  @UsePipes(getDefaultPipeValidator())
  @RabbitRPC({
    routingKey: accountUpdateUserProfileKey,
    queue: accountUpdateUserProfileKey,
    exchange: process.env.AMQP_EXCHANGE,
    errorHandler: replyErrorHandler,
  })
  async updateUserProfile(
    @RabbitPayload() payloadReq: AccountUpdateUserProfileRequest,
  ): Promise<AccountUpdateUserProfileResponse> {
    const payload = await this.userService.updateUserProfile(payloadReq)
    return {
      payload,
      status: ResponseStatuses.success,
    }
  }
}
