import { BadRequestException, Injectable } from '@nestjs/common'
import { IUserProfile } from '@gift/interfaces'
import { UserRepository } from './user.repository'
import { UserEntity } from './user.entity'
import { CreateUserDto } from '../auth/dtos/create-user.dto'
import { AmqpConnection } from '@golevelup/nestjs-rabbitmq'
import { ConfigService } from '@nestjs/config'
import {
  accViewRegisterEventKey,
  accViewUpdateUserProfileEventKey,
} from '@gift/contracts'

@Injectable()
export class UserService {
  constructor(
    private readonly userRepository: UserRepository,
    private readonly amqpConnection: AmqpConnection,
    private readonly configService: ConfigService,
  ) {}

  async getUserProfileById(userId: string): Promise<IUserProfile> {
    const user = await this.userRepository.findById(userId)
    if (!user) throw new BadRequestException('Пользователь не найден')
    const userEntity = new UserEntity(user)
    return userEntity.getUserProfile()
  }

  async createUser(data: { email: string; passwordHash: string }) {
    const userCandidate = await this.userRepository.findByEmail(data.email)
    if (userCandidate) {
      throw new BadRequestException('Такой пользователь уже зарегистрирован')
    }
    const newUser = await this.userRepository.create(new CreateUserDto(data))
    const userEntity = new UserEntity(newUser)
    this.amqpConnection.publish(
      this.configService.getOrThrow('AMQP_EXCHANGE'),
      accViewRegisterEventKey,
      userEntity.getUserEvent(),
    )
    return userEntity
  }

  async updateUserProfile(userProfile: IUserProfile): Promise<IUserProfile> {
    const user = await this.userRepository.findById(userProfile.userId)
    if (!user) throw new BadRequestException('Пользователь не найден')
    const userEntity = new UserEntity(user).updateProfile(userProfile)
    await this.userRepository.update(userEntity)
    this.amqpConnection.publish(
      this.configService.getOrThrow('AMQP_EXCHANGE'),
      accViewUpdateUserProfileEventKey,
      userEntity.getUserEvent(),
    )
    return userEntity.getUserProfile()
  }
}
