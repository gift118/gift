import {
  IEventUser,
  IUser,
  IUserInToken,
  IUserProfile,
  PartialBy,
} from '@gift/interfaces'
import { UnauthorizedException } from '@nestjs/common'
import { cloneDeep, escape } from 'lodash'

export class UserEntity implements IUser {
  userId: string
  email: string
  passwordHash: string
  isActive: boolean
  firstName: string | null
  lastName: string | null
  nickname: string | null
  bio: string | null

  constructor(user: IUser) {
    this.userId = user.userId
    this.email = user.email
    this.passwordHash = user.passwordHash
    this.isActive = user.isActive
    this.firstName = escape(user.firstName as string)
    this.lastName = escape(user.lastName as string)
    this.nickname = escape(user.nickname as string)
    this.bio = escape(user.bio as string)
  }

  updateProfile(userProfile: PartialBy<IUserProfile, 'userId'>): UserEntity {
    const cpUserProfile = cloneDeep(userProfile)
    delete cpUserProfile.userId
    return new UserEntity({ ...this, ...cpUserProfile })
  }

  getUserEvent(): IEventUser {
    return {
      userId: this.userId,
      email: this.email,
      isActive: this.isActive,
      firstName: this.firstName,
      lastName: this.lastName,
      nickname: this.nickname,
      bio: this.bio,
    }
  }

  getUserProfile(): IUserProfile {
    return {
      userId: this.userId,
      email: this.email,
      isActive: this.isActive,
      firstName: this.firstName,
      lastName: this.lastName,
      nickname: this.nickname,
      bio: this.bio,
    }
  }

  getUserForTokens(): IUserInToken {
    if (!this.userId) throw new UnauthorizedException()
    return {
      userId: this.userId,
    }
  }
}
