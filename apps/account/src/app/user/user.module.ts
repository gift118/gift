import { Module } from '@nestjs/common'
import { UserController } from './user.controller'
import { UserService } from './user.service'
import { PrismaModule } from '../prisma/prisma.module'
import { UserRepository } from './user.repository'
import { RabbitMQModule } from '@golevelup/nestjs-rabbitmq'
import { getRMQConfig } from '../configs/rmq.config'

@Module({
  imports: [
    PrismaModule,
    RabbitMQModule.forRootAsync(RabbitMQModule, getRMQConfig()),
  ],
  providers: [UserService, UserRepository, UserController],
  exports: [UserRepository, UserService],
})
export class UserModule {}
