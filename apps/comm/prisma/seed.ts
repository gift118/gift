import { Prisma, PrismaClient } from './client'
// @ts-ignore
import { dev } from '../../api/http/http-client.env.json'

const prisma = new PrismaClient()

const data: Prisma.CommCreateInput[] = [
  {
    entityId: dev.giftId,
    text: 'SUPER COMMENT',
    userId: dev.userId,
    commId: dev.commId,
  },
]

async function main() {
  console.log(`Start seeding ...`)
  for (const d of data) {
    const res = await prisma.comm.create({
      data: d,
    })
    console.log(`Created user with id: ${res.commId}`)
  }
  console.log(`Seeding finished.`)
}

main()
  .then(async () => {
    await prisma.$disconnect()
  })
  .catch(async (e) => {
    console.error(e)
    await prisma.$disconnect()
    process.exit(1)
  })
