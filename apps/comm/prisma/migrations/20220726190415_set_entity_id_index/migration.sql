-- DropIndex
DROP INDEX "comments_user_id_created_at_idx";

-- CreateIndex
CREATE INDEX "comments_entity_id_created_at_idx" ON "comments"("entity_id", "created_at" DESC);
