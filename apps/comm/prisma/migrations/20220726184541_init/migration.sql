-- CreateEnum
CREATE TYPE "Entity" AS ENUM ('GIFT');

-- CreateTable
CREATE TABLE "comments" (
    "comment_id" VARCHAR(36) NOT NULL,
    "user_id" VARCHAR(36) NOT NULL,
    "entity" "Entity" NOT NULL DEFAULT 'GIFT',
    "entity_id" VARCHAR(36) NOT NULL,
    "text" VARCHAR(280) NOT NULL,
    "created_at" TIMESTAMP(3) NOT NULL DEFAULT CURRENT_TIMESTAMP,
    "updated_at" TIMESTAMP(3) NOT NULL,

    CONSTRAINT "comments_pkey" PRIMARY KEY ("comment_id")
);

-- CreateIndex
CREATE INDEX "comments_user_id_created_at_idx" ON "comments"("user_id", "created_at" DESC);
