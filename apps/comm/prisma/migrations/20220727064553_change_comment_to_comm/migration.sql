/*
  Warnings:

  - You are about to drop the `comments` table. If the table is not empty, all the data it contains will be lost.

*/
-- DropTable
DROP TABLE "comments";

-- CreateTable
CREATE TABLE "comms" (
    "comm_id" VARCHAR(36) NOT NULL,
    "user_id" VARCHAR(36) NOT NULL,
    "entity" "Entity" NOT NULL DEFAULT 'GIFT',
    "entity_id" VARCHAR(36) NOT NULL,
    "text" VARCHAR(280) NOT NULL,
    "created_at" TIMESTAMP(3) NOT NULL DEFAULT CURRENT_TIMESTAMP,
    "updated_at" TIMESTAMP(3) NOT NULL,

    CONSTRAINT "comms_pkey" PRIMARY KEY ("comm_id")
);

-- CreateIndex
CREATE INDEX "comms_entity_id_created_at_idx" ON "comms"("entity_id", "created_at" DESC);
