import { Injectable } from '@nestjs/common'
import { IComm, ICreateComm, IGetCommBy, IPagindation } from '@gift/interfaces'
import { Comm, Prisma } from 'prisma/client'
import { PrismaService } from '../prisma/prisma.service'

@Injectable()
export class CommRepository {
  constructor(private readonly prismaService: PrismaService) {}

  async create(comm: ICreateComm): Promise<Comm> {
    return this.prismaService.comm.create({
      data: comm,
    })
  }

  async update({ commId, ...rest }: IComm): Promise<Comm> {
    return this.prismaService.comm.update({
      where: { commId },
      data: rest,
    })
  }

  delete(commId: string): Promise<Comm> {
    return this.prismaService.comm.delete({ where: { commId } })
  }

  findById(commId: string) {
    return this.prismaService.comm.findUnique({ where: { commId } })
  }

  findMany(query: IGetCommBy & IPagindation): Promise<Comm[]> {
    const where: Prisma.CommWhereInput = {}
    if (query.take && query.take > 50) query.take = 50
    if (query.userId) where.userId = query.userId
    if (query.commId) where.commId = query.commId
    if (query.entityId) where.entityId = query.entityId
    return this.prismaService.comm.findMany({
      skip: query.skip ?? 0,
      take: query.take ?? 10,
      where,
    })
  }
}
