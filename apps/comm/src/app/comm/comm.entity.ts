import { EntityType, IComm, PartialBy } from '@gift/interfaces'
import { cloneDeep, escape } from 'lodash'
import { AccViewCreateCommEvent } from '@gift/contracts'

export class CommEntity implements IComm {
  commId: string
  userId: string
  entityId: string
  entity: EntityType = 'GIFT'
  text: string

  constructor(comm: IComm) {
    this.commId = comm.commId
    this.userId = comm.userId
    this.entityId = comm.entityId ?? 'GIFT'
    this.entity = comm.entity
    this.text = escape(comm.text)
  }

  update(
    comm: PartialBy<IComm, 'entityId' | 'userId' | 'commId' | 'entity'>,
  ): CommEntity {
    const cpComm = cloneDeep(comm)
    delete cpComm.entity
    delete cpComm.commId
    delete cpComm.entityId
    delete cpComm.userId
    return new CommEntity({ ...this, ...cpComm })
  }

  getCommEventEvent(): AccViewCreateCommEvent {
    return {
      commId: this.commId,
      entity: this.entity,
      entityId: this.entityId,
      userId: this.userId,
      text: this.text,
    }
  }
}
