import { Module } from '@nestjs/common'
import { CommController } from './comm.controller'
import { CommService } from './comm.service'
import { PrismaModule } from '../prisma/prisma.module'
import { CommRepository } from './comm.repository'
import { RabbitMQModule } from '@golevelup/nestjs-rabbitmq'
import { getRMQConfig } from '../configs/rmq.config'

@Module({
  imports: [
    PrismaModule,
    RabbitMQModule.forRootAsync(RabbitMQModule, getRMQConfig()),
  ],
  providers: [CommController, CommService, CommRepository],
})
export class CommModule {}
