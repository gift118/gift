import { Injectable, UsePipes } from '@nestjs/common'
import { RabbitPayload, RabbitRPC } from '@golevelup/nestjs-rabbitmq'
import {
  commCreateCommKey,
  CommCreateCommRequest,
  CommCreateCommResponse,
  commGetCommsKey,
  CommGetCommsRequest,
  CommGetCommsResponse,
  commUpdateCommKey,
  CommUpdateCommRequest,
  CommUpdateCommResponse,
  ResponseStatuses,
} from '@gift/contracts'
import { getDefaultPipeValidator, replyErrorHandler } from '@gift/common'
import { CommService } from './comm.service'

@Injectable()
export class CommController {
  constructor(private readonly commService: CommService) {}

  @UsePipes(getDefaultPipeValidator())
  @RabbitRPC({
    routingKey: commCreateCommKey,
    queue: commCreateCommKey,
    exchange: process.env.AMQP_EXCHANGE,
    errorHandler: replyErrorHandler,
  })
  async create(
    @RabbitPayload() payloadReq: CommCreateCommRequest,
  ): Promise<CommCreateCommResponse> {
    const payload = await this.commService.create(payloadReq)
    return {
      payload,
      status: ResponseStatuses.success,
    }
  }

  @UsePipes(getDefaultPipeValidator())
  @RabbitRPC({
    routingKey: commUpdateCommKey,
    queue: commUpdateCommKey,
    exchange: process.env.AMQP_EXCHANGE,
    errorHandler: replyErrorHandler,
  })
  async update(
    @RabbitPayload() payloadReq: CommUpdateCommRequest,
  ): Promise<CommUpdateCommResponse> {
    const payload = await this.commService.update(payloadReq)
    return {
      payload,
      status: ResponseStatuses.success,
    }
  }

  @UsePipes(getDefaultPipeValidator())
  @RabbitRPC({
    routingKey: commGetCommsKey,
    queue: commGetCommsKey,
    exchange: process.env.AMQP_EXCHANGE,
    errorHandler: replyErrorHandler,
  })
  async get(
    @RabbitPayload() payloadReq: CommGetCommsRequest,
  ): Promise<CommGetCommsResponse> {
    const payload = await this.commService.get(payloadReq)
    return {
      payload,
      status: ResponseStatuses.success,
    }
  }
}
