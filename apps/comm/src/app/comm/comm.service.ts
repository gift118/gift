import { BadRequestException, Injectable } from '@nestjs/common'
import { IComm, ICreateComm, IGetCommBy } from '@gift/interfaces'
import { CommRepository } from './comm.repository'
import { CommEntity } from './comm.entity'
import { AmqpConnection } from '@golevelup/nestjs-rabbitmq'
import { accViewCreateCommKey, accViewUpdateCommKey } from '@gift/contracts'
import { ConfigService } from '@nestjs/config'

@Injectable()
export class CommService {
  constructor(
    private readonly commRepository: CommRepository,
    private readonly amqpConnection: AmqpConnection,
    private readonly configService: ConfigService,
  ) {}

  async create(comm: ICreateComm): Promise<IComm> {
    const createdComm = await this.commRepository.create(comm)
    const commEntity = new CommEntity(createdComm)
    this.amqpConnection.publish(
      this.configService.getOrThrow('AMQP_EXCHANGE'),
      accViewCreateCommKey,
      commEntity.getCommEventEvent(),
    )
    return createdComm
  }

  async get(query: IGetCommBy): Promise<IComm[]> {
    return this.commRepository.findMany(query)
  }

  async update(comm: IComm): Promise<IComm> {
    const commFromDb = await this.commRepository.findById(comm.commId)
    if (!commFromDb) throw new BadRequestException('Comment not found')
    const commEntity = new CommEntity(commFromDb).update(comm)
    this.amqpConnection.publish(
      this.configService.getOrThrow('AMQP_EXCHANGE'),
      accViewUpdateCommKey,
      commEntity.getCommEventEvent(),
    )
    return this.commRepository.update(commEntity)
  }
}
