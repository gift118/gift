import { Module } from '@nestjs/common'
import { ConfigModule } from '@nestjs/config'
import { CommModule } from './comm/comm.module'
import { PrismaModule } from './prisma/prisma.module'

@Module({
  imports: [ConfigModule.forRoot({ isGlobal: true }), CommModule, PrismaModule],
})
export class AppModule {}
