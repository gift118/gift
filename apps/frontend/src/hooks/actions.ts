import { useDispatch } from 'react-redux'
import { bindActionCreators } from '@reduxjs/toolkit'
import { accountAuthSlice } from '../store/account/auth/account.auth.slice'

const actions = {
  ...accountAuthSlice.actions,
}

export const useActions = () => {
  const dispatch = useDispatch()
  return bindActionCreators(actions, dispatch)
}
