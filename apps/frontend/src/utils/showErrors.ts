import { message } from 'antd'
import { FetchBaseQueryError } from '@reduxjs/toolkit/query'
import { IApiError } from '@gift/contracts'

export const showErrors = (error: FetchBaseQueryError | undefined) => {
  if (!error?.data) return
  const apiError = error.data as IApiError
  let messages = apiError.message
  if (!Array.isArray(messages)) messages = [messages]
  messages.forEach((m) => message.error(m))
}
