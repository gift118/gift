import { Rule } from 'rc-field-form/lib/interface'

export const rules = {
  required: (message = 'Обязательное поле'): Rule => ({
    required: true,
    message,
  }),
  min: (min: number): Rule => ({
    required: true,
    message: `Минимум ${min} символов`,
    min,
  }),
  max: (max: number): Rule => ({
    required: true,
    message: `Максимум ${max} символов`,
    max,
  }),
  email: (message = 'Введите корректную почту'): Rule => ({
    type: 'email',
    message,
  }),
}
