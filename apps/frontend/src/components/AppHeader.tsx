import React, { FC } from 'react'
import type { MenuProps } from 'antd'
import { Col, Image, Layout, Menu, Row, Skeleton } from 'antd'
import { useNavigate } from 'react-router-dom'
import { RoutesEnum } from '../router/routes'
import { useAppSelector } from '../hooks/redux'
import { useLogoutMutation } from '../store/account/auth/account.auth.api'

interface Props {
  isLoading: boolean
}

export const AppHeader: FC<Props> = ({ isLoading }) => {
  const { isAuth } = useAppSelector((state) => state.accountAuth)
  const { user } = useAppSelector((state) => state.accView)
  const [logout] = useLogoutMutation()
  const navigate = useNavigate()

  const privateMenuItems: MenuProps['items'] = [
    {
      key: RoutesEnum.LOGIN,
      label: 'Выйти',
      onClick: () => {
        logout()
        navigate(RoutesEnum.LOGIN)
      },
    },
  ]

  const publicMenuItems: MenuProps['items'] = [
    {
      key: RoutesEnum.LOGIN,
      label: 'Войти',
      onClick: () => navigate(RoutesEnum.LOGIN),
    },
    {
      key: RoutesEnum.REGISTER,
      label: 'Регистрация',
      onClick: () => navigate(RoutesEnum.REGISTER),
    },
  ]

  return (
    <Layout.Header>
      <Row justify="space-between">
        <Col>
          <Image preview={false} width={40} src="/assets/logo.png" />
        </Col>
        <Col flex="auto">
          {isLoading ? (
            <Row justify="end" align={'bottom'}>
              <Skeleton.Input
                active
                style={{ marginTop: 15, marginRight: 10 }}
              />
              <Skeleton.Input active style={{ marginTop: 15 }} />
            </Row>
          ) : (
            <Row justify="end">
              <div>{user.nickname || user.email}</div>
              <Menu
                style={{
                  justifyContent: 'flex-end',
                  width: isAuth ? '' : '100%',
                }}
                mode="horizontal"
                theme="dark"
                selectable={false}
                items={isAuth ? privateMenuItems : publicMenuItems}
              />
            </Row>
          )}
        </Col>
      </Row>
    </Layout.Header>
  )
}
