import React, { FC } from 'react'
import { Button, Form, Input, Row } from 'antd'
import { rules } from '../utils/rules'
import { IGift, PartialBy } from '@gift/interfaces'
import { useAppSelector } from '../hooks/redux'

interface Props {
  submit: (gift: PartialBy<IGift, 'giftId'>) => void
  setIsModalOpen: (bool: boolean) => void
  isLoading: boolean
  okButtonText: string
  giftForm: Pick<IGift, 'title' | 'text'>
}

export const GiftForm: FC<Props> = ({
  submit,
  setIsModalOpen,
  isLoading,
  okButtonText,
  giftForm,
}) => {
  const { userId } = useAppSelector((state) => state.accView.user)

  const onFinish = async (giftForm: Pick<IGift, 'title' | 'text'>) => {
    await submit({ ...giftForm, userId })
    setIsModalOpen(false)
  }

  return (
    <Form
      name="gift"
      labelCol={{ span: 5 }}
      onFinish={onFinish}
      initialValues={giftForm}
    >
      <Form.Item
        label="Заголовок"
        name="title"
        rules={[rules.required(), rules.max(12)]}
      >
        <Input />
      </Form.Item>

      <Form.Item
        label="Я хочу"
        name="text"
        rules={[rules.required(), rules.max(280)]}
      >
        <Input.TextArea showCount />
      </Form.Item>
      <Row justify="end">
        <Form.Item>
          <Button type="primary" htmlType="submit" loading={isLoading}>
            {okButtonText}
          </Button>
        </Form.Item>
      </Row>
    </Form>
  )
}
