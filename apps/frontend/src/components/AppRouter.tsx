import React, { FC } from 'react'
import { Route, Routes } from 'react-router-dom'
import { Col, Row, Spin } from 'antd'
import { Login } from '../pages/Login'
import { Register } from '../pages/Register'
import { Main } from '../pages/Main'
import { RoutesEnum } from '../router/routes'
import { Container } from './container/Container'
import { useAppSelector } from '../hooks/redux'

interface Props {
  isLoading: boolean
}

export const AppRouter: FC<Props> = ({ isLoading }) => {
  const { isAuth } = useAppSelector((state) => state.accountAuth)

  if (isLoading)
    return (
      <Container>
        <Row
          justify="center"
          align="middle"
          className="h100"
          style={{ width: '100%' }}
        >
          <Col>
            <Spin size="large" />
          </Col>
        </Row>
      </Container>
    )

  return (
    <>
      {isAuth ? (
        <Routes>
          <Route
            path={RoutesEnum.MAIN}
            element={
              <Container>
                <Main />
              </Container>
            }
          />
          <Route
            path="*"
            element={
              <Container>
                <Main />
              </Container>
            }
          />
        </Routes>
      ) : (
        <Routes>
          <Route
            path={RoutesEnum.LOGIN}
            element={
              <Container>
                <Login />
              </Container>
            }
          />
          <Route
            path={RoutesEnum.REGISTER}
            element={
              <Container>
                <Register />
              </Container>
            }
          />
          <Route
            path="*"
            element={
              <Container>
                <Login />
              </Container>
            }
          />
        </Routes>
      )}
    </>
  )
}
