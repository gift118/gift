import React, { useState } from 'react'
import { Button, Form, Input, Row } from 'antd'
import { rules } from '../utils/rules'
import { useLoginMutation } from '../store/account/auth/account.auth.api'
import { AccountLoginRequest } from '@gift/contracts'

export const LoginForm = () => {
  const [loginForm, setLoginForm] = useState<AccountLoginRequest>({
    email: '',
    password: '',
  })
  const [login, { isLoading }] = useLoginMutation()

  const setEmail = (email: string) => {
    setLoginForm({
      ...loginForm,
      email,
    })
  }
  const setPassword = (password: string) => {
    setLoginForm({
      ...loginForm,
      password,
    })
  }

  const onFinish = (values: AccountLoginRequest) => {
    login(values)
  }
  return (
    <Form
      name="login"
      labelCol={{ span: 8 }}
      wrapperCol={{ span: 16 }}
      onFinish={onFinish}
      autoComplete="off"
    >
      <Form.Item
        label="Почта"
        name="email"
        rules={[rules.email(), rules.required()]}
      >
        <Input
          value={loginForm.email}
          onChange={(e) => setEmail(e.target.value)}
        />
      </Form.Item>

      <Form.Item
        label="Пароль"
        name="password"
        rules={[rules.required(), rules.min(8)]}
      >
        <Input.Password
          value={loginForm.password}
          onChange={(e) => setPassword(e.target.value)}
        />
      </Form.Item>
      <Row justify="end">
        <Form.Item>
          <Button type="primary" htmlType="submit" loading={isLoading}>
            Войти
          </Button>
        </Form.Item>
      </Row>
    </Form>
  )
}
