import React, { useState } from 'react'
import { Button, Modal } from 'antd'
import { useCreateGiftMutation } from '../store/gift/gift.api'
import { GiftForm } from './GiftForm'
import { IGift } from '@gift/interfaces'

export const AddGift = () => {
  const [createGift, { isLoading }] = useCreateGiftMutation()
  const [isModalOpen, setIsModalOpen] = useState(false)
  const [giftForm, _] = useState<Pick<IGift, 'title' | 'text'>>({
    title: '',
    text: '',
  })

  return (
    <>
      <Button type="primary" block onClick={() => setIsModalOpen(true)}>
        Ещё я хочу
      </Button>

      <Modal
        title="Подарок"
        visible={isModalOpen}
        footer={null}
        onCancel={() => setIsModalOpen(false)}
      >
        <GiftForm
          okButtonText="Добавить"
          giftForm={giftForm}
          isLoading={isLoading}
          setIsModalOpen={setIsModalOpen}
          submit={createGift}
        />
      </Modal>
    </>
  )
}
