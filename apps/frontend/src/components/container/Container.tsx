import React from 'react'
import './container.css'

export const Container = (props: { children: JSX.Element }) => {
  return <div className="container">{props.children}</div>
}
