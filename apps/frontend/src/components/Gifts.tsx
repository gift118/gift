import React, { FC, useState } from 'react'
import { useAppSelector } from '../hooks/redux'
import { Card, Modal, Space, Typography } from 'antd'
import { IGift, IGiftView, PartialBy } from '@gift/interfaces'
import { EditOutlined } from '@ant-design/icons'
import { GiftForm } from './GiftForm'
import { useUpdateGiftMutation } from '../store/gift/gift.api'

const Gift: FC<IGiftView> = ({ title, text, giftId }) => {
  const [updateGift, { isLoading }] = useUpdateGiftMutation()
  const [isModalOpen, setIsModalOpen] = useState(false)
  const [giftForm, _] = useState<Pick<IGift, 'title' | 'text'>>({
    title,
    text,
  })

  const submit = (gift: PartialBy<IGift, 'giftId'>) => {
    updateGift({
      ...gift,
      giftId,
    })
  }

  return (
    <Card
      title={title}
      size="small"
      style={{ width: '100%' }}
      actions={[<EditOutlined onClick={() => setIsModalOpen(true)} />]}
    >
      <Typography style={{ wordBreak: 'break-all' }}>{text}</Typography>
      <Modal
        title="Подарок"
        visible={isModalOpen}
        footer={null}
        onCancel={() => setIsModalOpen(false)}
      >
        <GiftForm
          okButtonText="Обновить"
          giftForm={giftForm}
          isLoading={isLoading}
          setIsModalOpen={setIsModalOpen}
          submit={submit}
        />
      </Modal>
    </Card>
  )
}

export const Gifts = () => {
  const { gifts } = useAppSelector((state) => state.accView)

  if (!gifts.length) return <div>нет подарков</div>

  return (
    <Space
      direction="vertical"
      size="middle"
      style={{ display: 'flex', marginTop: 15 }}
    >
      {gifts.map((g) => (
        <Gift {...g} key={g.giftId} />
      ))}
    </Space>
  )
}
