import React, { useState } from 'react'
import { Button, Form, Input, Row } from 'antd'
import { rules } from '../utils/rules'
import { AccountRegisterRequest } from '@gift/contracts'
import { useRegisterMutation } from '../store/account/auth/account.auth.api'

export const RegistrationForm = () => {
  const [registerForm, setRegisterForm] = useState<AccountRegisterRequest>({
    email: '',
    password: '',
  })
  const [register, { isLoading }] = useRegisterMutation()

  const setEmail = (email: string) => {
    setRegisterForm({
      ...registerForm,
      email,
    })
  }

  const setPassword = (password: string) => {
    setRegisterForm({
      ...registerForm,
      password,
    })
  }

  const onFinish = (values: AccountRegisterRequest) => {
    register(values)
  }

  return (
    <Form
      name="register"
      labelCol={{ span: 8 }}
      wrapperCol={{ span: 16 }}
      onFinish={onFinish}
      autoComplete="off"
    >
      <Form.Item
        label="Почта"
        name="email"
        rules={[rules.email(), rules.required(), rules.min(8)]}
      >
        <Input
          value={registerForm.email}
          onChange={(e) => setEmail(e.target.value)}
        />
      </Form.Item>

      <Form.Item
        label="Пароль"
        name="password"
        rules={[rules.required(), rules.min(8)]}
      >
        <Input.Password
          value={registerForm.password}
          onChange={(e) => setPassword(e.target.value)}
        />
      </Form.Item>
      <Row justify="end">
        <Form.Item>
          <Button type="primary" htmlType="submit" loading={isLoading}>
            Зарегестироваться
          </Button>
        </Form.Item>
      </Row>
    </Form>
  )
}
