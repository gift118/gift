import React from 'react'
import { Layout } from 'antd'

export const AppFooter = () => {
  return <Layout.Footer />
}
