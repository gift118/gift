import React from 'react'
import { RegistrationForm } from '../components/RegistrationForm'
import { Card, Layout, Row } from 'antd'

export const Register = () => {
  return (
    <Layout>
      <Row justify="center" align="middle" className="h100">
        <Card>
          <RegistrationForm />
        </Card>
      </Row>
    </Layout>
  )
}
