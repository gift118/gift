import React from 'react'
import { useGetAccViewQuery } from '../store/acc-view/acc-view.api'
import { Layout, Row, Skeleton } from 'antd'
import { Gifts } from '../components/Gifts'
import { AddGift } from '../components/AddGift'

export const Main = () => {
  const { isLoading } = useGetAccViewQuery()

  if (isLoading)
    return (
      <Row justify="end" style={{ width: '100%', marginTop: 15 }}>
        <Skeleton />
        <Skeleton />
        <Skeleton />
      </Row>
    )
  return (
    <Layout.Content style={{ marginTop: 15 }}>
      <AddGift />
      <Gifts />
    </Layout.Content>
  )
}
