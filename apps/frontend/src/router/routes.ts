export enum RoutesEnum {
  LOGIN = '/login',
  REGISTER = '/register',
  MAIN = '/',
}
