import React, { useEffect } from 'react'
import { Layout } from 'antd'
import { AppHeader } from './components/AppHeader'
import { AppFooter } from './components/AppFooter'
import { AppRouter } from './components/AppRouter'
import { useRefreshMutation } from './store/account/auth/account.auth.api'

export const App = () => {
  const [refresh, { isLoading }] = useRefreshMutation()

  useEffect(() => {
    if (
      !localStorage.getItem(
        process.env.REACT_APP_ACCESS_TOKEN_LS_NAME as string,
      )
    )
      return
    refresh()
  }, [])

  return (
    <Layout>
      <AppHeader isLoading={isLoading} />
      <AppRouter isLoading={isLoading} />
      <AppFooter />
    </Layout>
  )
}
