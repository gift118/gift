import type {
  BaseQueryFn,
  FetchArgs,
  FetchBaseQueryError,
} from '@reduxjs/toolkit/query'
import { fetchBaseQuery } from '@reduxjs/toolkit/query'
import { ITokens } from '@gift/interfaces'
import { Mutex } from 'async-mutex'
import { accountAuthSlice } from './account/auth/account.auth.slice'

const mutex = new Mutex()

const baseQuery = fetchBaseQuery({
  baseUrl: process.env.REACT_APP_API_HOST,
  credentials: 'include',
  prepareHeaders(headers) {
    const accessToken = localStorage.getItem(
      process.env.REACT_APP_ACCESS_TOKEN_LS_NAME as string,
    )
    if (accessToken) {
      headers.set('authorization', `Bearer ${accessToken}`)
    }
    return headers
  },
})

export const baseQueryWithReAuth: BaseQueryFn<
  string | FetchArgs,
  unknown,
  FetchBaseQueryError
> = async (args, api, extraOptions) => {
  await mutex.waitForUnlock()
  let result = await baseQuery(args, api, extraOptions)
  if (result.error && result.error.status === 401) {
    if (!mutex.isLocked()) {
      const release = await mutex.acquire()
      try {
        const refreshResult = await baseQuery(
          { url: '/auth/refresh', method: 'POST' },
          api,
          extraOptions,
        )
        if (refreshResult.data) {
          api.dispatch(
            accountAuthSlice.actions.refreshToken(
              refreshResult.data as ITokens,
            ),
          )
          result = await baseQuery(args, api, extraOptions)
        } else {
          api.dispatch(accountAuthSlice.actions.logout())
        }
      } finally {
        release()
      }
    } else {
      await mutex.waitForUnlock()
      result = await baseQuery(args, api, extraOptions)
    }
  }
  return result
}
