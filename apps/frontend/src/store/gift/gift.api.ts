import { createApi } from '@reduxjs/toolkit/query/react'
import { GiftCreateGiftRequest, GiftUpdateGiftRequest } from '@gift/contracts'
import { baseQueryWithReAuth } from '../baseQueryWithReAuth'
import { IGift } from '@gift/interfaces'

export const giftApi = createApi({
  reducerPath: 'gift.api',
  baseQuery: baseQueryWithReAuth,
  endpoints: (build) => ({
    createGift: build.mutation<IGift, GiftCreateGiftRequest>({
      query: (body) => ({
        url: `/gifts`,
        method: 'POST',
        body,
      }),
    }),
    updateGift: build.mutation<IGift, GiftUpdateGiftRequest>({
      query: (body) => ({
        url: `/gifts`,
        method: 'PATCH',
        body,
      }),
    }),
  }),
})

export const { useCreateGiftMutation, useUpdateGiftMutation } = giftApi
