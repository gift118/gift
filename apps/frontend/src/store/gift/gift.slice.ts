import { createSlice } from '@reduxjs/toolkit'
import { showErrors } from '../../utils/showErrors'
import { giftApi } from './gift.api'

export const giftSlice = createSlice({
  name: 'gift.slice',
  initialState: {},
  reducers: {},
  extraReducers: (builder) => {
    builder
      .addMatcher(
        giftApi.endpoints.createGift.matchRejected,
        (state, { payload }) => {
          showErrors(payload)
        },
      )
      .addMatcher(
        giftApi.endpoints.updateGift.matchRejected,
        (state, { payload }) => {
          showErrors(payload)
        },
      )
  },
})
