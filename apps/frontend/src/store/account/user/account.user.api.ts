import { createApi } from '@reduxjs/toolkit/query/react'
import { baseQueryWithReAuth } from '../../baseQueryWithReAuth'

export const accountUserApi = createApi({
  reducerPath: 'account.user.api',
  baseQuery: baseQueryWithReAuth,
  endpoints: (build) => ({}),
})

export const {} = accountUserApi
