import { createSlice, PayloadAction } from '@reduxjs/toolkit'
import { ITokens } from '@gift/interfaces'
import { accountAuthApi } from './account.auth.api'
import { showErrors } from '../../../utils/showErrors'

interface IAccountAuthState {
  isAuth: boolean
}

const initialState: IAccountAuthState = {
  isAuth: false,
}

export const accountAuthSlice = createSlice({
  name: 'account.auth.slice',
  initialState,
  reducers: {
    refreshToken(state, { payload }: PayloadAction<ITokens>) {
      state.isAuth = true
      localStorage.setItem(
        process.env.REACT_APP_ACCESS_TOKEN_LS_NAME as string,
        payload.accessToken,
      )
    },
    logout(state) {
      state.isAuth = false
      localStorage.removeItem(
        process.env.REACT_APP_ACCESS_TOKEN_LS_NAME as string,
      )
    },
  },
  extraReducers: (builder) => {
    builder
      .addMatcher(
        accountAuthApi.endpoints.login.matchRejected,
        (state, { payload }) => {
          showErrors(payload)
          state.isAuth = false
        },
      )
      .addMatcher(
        accountAuthApi.endpoints.login.matchFulfilled,
        (state, { payload }) => {
          if (!payload) return
          state.isAuth = true
          localStorage.setItem(
            process.env.REACT_APP_ACCESS_TOKEN_LS_NAME as string,
            payload.accessToken,
          )
        },
      )
      .addMatcher(
        accountAuthApi.endpoints.register.matchRejected,
        (state, { payload }) => {
          showErrors(payload)
        },
      )
      .addMatcher(
        accountAuthApi.endpoints.register.matchFulfilled,
        (state, { payload }) => {
          if (!payload) return
          state.isAuth = true
          localStorage.setItem(
            process.env.REACT_APP_ACCESS_TOKEN_LS_NAME as string,
            payload.accessToken,
          )
        },
      )
      .addMatcher(
        accountAuthApi.endpoints.logout.matchRejected,
        (state, { payload }) => {
          showErrors(payload)
          state.isAuth = false
          localStorage.removeItem(
            process.env.REACT_APP_ACCESS_TOKEN_LS_NAME as string,
          )
        },
      )
      .addMatcher(accountAuthApi.endpoints.logout.matchFulfilled, (state) => {
        state.isAuth = false
        localStorage.removeItem(
          process.env.REACT_APP_ACCESS_TOKEN_LS_NAME as string,
        )
      })
      .addMatcher(
        accountAuthApi.endpoints.refresh.matchRejected,
        (state, { payload }) => {
          showErrors(payload)
          state.isAuth = false
        },
      )
      .addMatcher(
        accountAuthApi.endpoints.refresh.matchFulfilled,
        (state, { payload }) => {
          if (!payload) return
          state.isAuth = true
          localStorage.setItem(
            process.env.REACT_APP_ACCESS_TOKEN_LS_NAME as string,
            payload.accessToken,
          )
        },
      )
  },
})
