import { createApi } from '@reduxjs/toolkit/query/react'
import {
  AccountLoginRequest,
  AccountLogoutResponse,
  AccountRegisterRequest,
} from '@gift/contracts'
import { ITokens } from '@gift/interfaces'
import { baseQueryWithReAuth } from '../../baseQueryWithReAuth'

export const accountAuthApi = createApi({
  reducerPath: 'account.auth.api',
  baseQuery: baseQueryWithReAuth,
  endpoints: (build) => ({
    login: build.mutation<ITokens, AccountRegisterRequest>({
      query: (body) => ({
        url: `/auth/login`,
        method: 'POST',
        body,
      }),
    }),
    register: build.mutation<ITokens, AccountLoginRequest>({
      query: (body) => ({
        url: `/auth/register`,
        method: 'POST',
        body,
      }),
    }),
    logout: build.mutation<AccountLogoutResponse, void>({
      query: () => ({
        url: `/auth/logout`,
        method: 'POST',
      }),
    }),
    refresh: build.mutation<ITokens, void>({
      query: () => ({
        url: `/auth/refresh`,
        method: 'POST',
      }),
    }),
  }),
})

export const {
  useLoginMutation,
  useRegisterMutation,
  useRefreshMutation,
  useLogoutMutation,
} = accountAuthApi
