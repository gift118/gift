import { configureStore } from '@reduxjs/toolkit'
import { accountAuthSlice } from './account/auth/account.auth.slice'
import { accountAuthApi } from './account/auth/account.auth.api'
import { accViewSlice } from './acc-view/acc-view.slice'
import { accViewApi } from './acc-view/acc-view.api'
import { giftSlice } from './gift/gift.slice'
import { giftApi } from './gift/gift.api'
import { accountUserApi } from './account/user/account.user.api'
import { accountUserSlice } from './account/user/account.user.slice'

export const store = configureStore({
  reducer: {
    accountAuth: accountAuthSlice.reducer,
    accView: accViewSlice.reducer,
    gift: giftSlice.reducer,
    accountUser: accountUserSlice.reducer,
    [accountAuthApi.reducerPath]: accountAuthApi.reducer,
    [accViewApi.reducerPath]: accViewApi.reducer,
    [giftApi.reducerPath]: giftApi.reducer,
    [accountUserApi.reducerPath]: accountUserApi.reducer,
  },
  middleware: (getDefaultMiddleware) =>
    getDefaultMiddleware()
      .concat(accountAuthApi.middleware)
      .concat(accViewApi.middleware)
      .concat(accountUserApi.middleware)
      .concat(giftApi.middleware),
})

export type RootState = ReturnType<typeof store.getState>
