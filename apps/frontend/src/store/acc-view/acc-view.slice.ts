import { createSlice } from '@reduxjs/toolkit'
import { IAccView } from '@gift/interfaces'
import { showErrors } from '../../utils/showErrors'
import { accViewApi } from './acc-view.api'
import { giftApi } from '../gift/gift.api'
import { accountAuthApi } from '../account/auth/account.auth.api'

const initialState: IAccView = {
  gifts: [],
  user: {
    userId: '',
    nickname: null,
    firstName: null,
    lastName: null,
    bio: null,
    email: '',
  },
}

export const accViewSlice = createSlice({
  name: 'acc-view.slice',
  initialState,
  reducers: {},
  extraReducers: (builder) => {
    builder
      .addMatcher(
        accViewApi.endpoints.getAccView.matchRejected,
        (state, { payload }) => {
          showErrors(payload)
        },
      )
      .addMatcher(
        accViewApi.endpoints.getAccView.matchFulfilled,
        (state, { payload }) => {
          state.user = payload.user
          state.gifts = payload.gifts
        },
      )
      .addMatcher(
        giftApi.endpoints.createGift.matchFulfilled,
        (state, { payload }) => {
          state.gifts.unshift({ ...payload, comms: [] })
        },
      )
      .addMatcher(
        giftApi.endpoints.updateGift.matchFulfilled,
        (state, { payload }) => {
          const candidateGiftIdx = state.gifts.findIndex(
            (g) => g.giftId === payload.giftId,
          )
          if (candidateGiftIdx === -1) return
          state.gifts[candidateGiftIdx] = {
            ...payload,
            comms: state.gifts[candidateGiftIdx].comms,
          }
        },
      )
      .addMatcher(accountAuthApi.endpoints.logout.matchFulfilled, (state) => {
        state.gifts = initialState.gifts
        state.user = initialState.user
      })
  },
})
