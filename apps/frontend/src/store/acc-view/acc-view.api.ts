import { createApi } from '@reduxjs/toolkit/query/react'
import { IAccView } from '@gift/interfaces'
import { baseQueryWithReAuth } from '../baseQueryWithReAuth'

export const accViewApi = createApi({
  reducerPath: 'acc-view.api',
  baseQuery: baseQueryWithReAuth,
  endpoints: (build) => ({
    getAccView: build.query<IAccView, void>({
      query: () => ({
        url: `/`,
      }),
    }),
  }),
})

export const { useGetAccViewQuery } = accViewApi
