import { Prop, Schema, SchemaFactory } from '@nestjs/mongoose'
import { IAccView, ICommView, IGiftView, IUserView } from '@gift/interfaces'
import { Document } from 'mongoose'

@Schema()
export class CommView extends Document implements ICommView {
  @Prop({ required: true })
  commId: string

  @Prop({ required: true })
  userId: string

  @Prop({ required: true })
  text: string

  @Prop({ type: String })
  firstName: string | null

  @Prop({ type: String })
  lastName: string | null

  @Prop({ type: String })
  nickname: string | null
}
export const CommViewSchema = SchemaFactory.createForClass(CommView)

@Schema()
export class GiftView extends Document implements IGiftView {
  @Prop({ type: [CommViewSchema], _id: false })
  comms: CommView[]

  @Prop({ required: true })
  text: string

  @Prop({ required: true })
  giftId: string

  @Prop({ required: true })
  title: string
}
export const GiftViewSchema = SchemaFactory.createForClass(GiftView)

@Schema()
export class UserView extends Document implements IUserView {
  @Prop({ required: true })
  userId: string

  @Prop({ type: String })
  lastName: string | null

  @Prop({ type: String })
  firstName: string | null

  @Prop({ type: String })
  nickname: string | null

  @Prop({ type: String })
  bio: string | null

  @Prop({ required: true })
  email: string
}
export const UserViewSchema = SchemaFactory.createForClass(UserView)

@Schema({ collection: 'AccView' })
export class AccView extends Document implements IAccView {
  @Prop({ type: UserViewSchema, _id: false })
  user: IUserView

  @Prop({ type: [GiftViewSchema], _id: false })
  gifts: IGiftView[]
}
export const AccViewSchema = SchemaFactory.createForClass(AccView)
AccViewSchema.index({ 'user.userId': 1 }, { unique: true })
