import { Module } from '@nestjs/common'
import { MongooseModule } from '@nestjs/mongoose'
import { AccViewController } from './acc-view.controller'
import { AccViewService } from './acc-view.service'
import { AccViewRepository } from './acc-view.repository'
import { AccView, AccViewSchema } from './acc-view.model'
import { RabbitMQModule } from '@golevelup/nestjs-rabbitmq'
import { getRMQConfig } from '../configs/rmq.config'

@Module({
  imports: [
    MongooseModule.forFeature([{ name: AccView.name, schema: AccViewSchema }]),
    RabbitMQModule.forRootAsync(RabbitMQModule, getRMQConfig()),
  ],
  providers: [AccViewController, AccViewService, AccViewRepository],
})
export class AccViewModule {}
