import { HttpException, Injectable } from '@nestjs/common'
import { AccViewRepository } from './acc-view.repository'
import { AccViewEntity } from './acc-view.entity'
import {
  accountGetUserProfileKey,
  AccountGetUserProfileResponse,
  AccViewCreateCommEvent,
  AccViewCreateGiftEvent,
  AccViewGetAccViewRequest,
  AccViewRegisterEvent,
  AccViewUpdateCommEvent,
  AccViewUpdateGiftEvent,
  AccViewUpdateUserProfileEvent,
} from '@gift/contracts'
import { AmqpConnection } from '@golevelup/nestjs-rabbitmq'
import { ConfigService } from '@nestjs/config'
import { isError } from '@gift/common'
import { IAccView } from '@gift/interfaces'

@Injectable()
export class AccViewService {
  constructor(
    private readonly accViewRepository: AccViewRepository,
    private readonly amqpConnection: AmqpConnection,
    private readonly configService: ConfigService,
  ) {}

  async create(accountRegisterEvent: AccViewRegisterEvent) {
    const accViewEntity = new AccViewEntity({
      user: accountRegisterEvent,
      gifts: [],
    })
    return this.accViewRepository.create(accViewEntity)
  }

  async update(user: AccViewUpdateUserProfileEvent) {
    await this.updateMainUser(user)
    await this.updateCommsUser(user)
  }

  async updateGift(gift: AccViewUpdateGiftEvent) {
    const accView = await this.accViewRepository.getAccViewByUserId(gift.userId)
    const accViewEntity = new AccViewEntity(accView)
    accViewEntity.updateGift(gift)
    await this.accViewRepository.update(accViewEntity)
  }

  async createGift(gift: AccViewCreateGiftEvent) {
    const accView = await this.accViewRepository.getAccViewByUserId(gift.userId)
    const accViewEntity = new AccViewEntity(accView)
    accViewEntity.createGift(gift)
    await this.accViewRepository.update(accViewEntity)
  }

  async createComm(comm: AccViewCreateCommEvent) {
    const accView = await this.accViewRepository.getAccViewByUserId(comm.userId)
    const accViewEntity = new AccViewEntity(accView)

    const res =
      await this.amqpConnection.request<AccountGetUserProfileResponse>({
        exchange: this.configService.getOrThrow('AMQP_EXCHANGE'),
        routingKey: accountGetUserProfileKey,
        payload: { userId: comm.userId },
      })
    if (isError(res)) {
      throw new HttpException(res.error, res.error.statusCode)
    }
    accViewEntity.createComm(comm, res.payload)
    await this.accViewRepository.update(accViewEntity)
  }

  async updateComm(comm: AccViewUpdateCommEvent) {
    const accView = await this.accViewRepository.getAccViewByUserId(comm.userId)
    const accViewEntity = new AccViewEntity(accView.toObject())
    accViewEntity.updateComm(comm)
    await this.accViewRepository.update(accViewEntity)
  }

  private async updateMainUser(user: AccViewUpdateUserProfileEvent) {
    const accView = await this.accViewRepository.getAccViewByUserId(user.userId)
    const accViewEntity = new AccViewEntity(accView)
    accViewEntity.updateUser(user)
    await this.accViewRepository.update(accViewEntity)
  }

  private async updateCommsUser(user: AccViewUpdateUserProfileEvent) {
    const accViews = await this.accViewRepository.getAccViewsByCommUserId(
      user.userId,
    )
    for (const accView of accViews) {
      const accViewEntity = new AccViewEntity(accView)
      accViewEntity.updateCommUser(user)
      await this.accViewRepository.update(accViewEntity)
    }
  }

  async getAccView(user: AccViewGetAccViewRequest): Promise<IAccView> {
    const accView = await this.accViewRepository.getAccViewByUserId(user.userId)
    return new AccViewEntity(accView)
  }
}
