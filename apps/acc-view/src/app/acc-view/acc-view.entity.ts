import { IAccView, IGiftView, IUserProfile, IUserView } from '@gift/interfaces'
import {
  AccViewCreateCommEvent,
  AccViewUpdateCommEvent,
  AccViewUpdateGiftEvent,
  AccViewUpdateUserProfileEvent,
} from '@gift/contracts'
import { NotFoundException } from '@nestjs/common'

export class AccViewEntity implements IAccView {
  user: IUserView
  gifts: IGiftView[]

  constructor(accView: IAccView) {
    this.gifts = accView.gifts
    this.user = accView.user
  }

  updateUser(user: AccViewUpdateUserProfileEvent) {
    this.user = user
  }

  updateGift(gift: AccViewUpdateGiftEvent) {
    const giftIdx = this.getGiftIdxByGiftId(gift.giftId)
    if (giftIdx === -1) throw new NotFoundException('Подарка не найдено.')
    this.gifts[giftIdx] = {
      ...this.gifts[giftIdx],
      ...gift,
    }
  }

  createGift(gift: AccViewUpdateGiftEvent) {
    const giftIdx = this.getGiftIdxByGiftId(gift.giftId)
    if (giftIdx !== -1) throw new NotFoundException('Подарок существует')
    this.gifts.unshift({
      ...gift,
      comms: [],
    })
  }

  updateComm(comm: AccViewUpdateCommEvent) {
    const giftIdx = this.getGiftIdxByGiftId(comm.entityId)
    if (giftIdx === -1) throw new NotFoundException('Подарка не найдено')
    const commIdx = this.getCommIdxByCommId(comm.commId, this.gifts[giftIdx])
    if (commIdx === -1) throw new NotFoundException('Комментарий не существует')
    this.gifts[giftIdx].comms[commIdx] = {
      ...this.gifts[giftIdx].comms[commIdx],
      text: comm.text,
    }
  }

  updateCommUser(user: AccViewUpdateUserProfileEvent) {
    this.gifts.forEach((g) => {
      g.comms = g.comms.map((c) => {
        if (user.userId !== c.userId) {
          return c
        }
        return {
          ...c,
          lastName: user.lastName,
          firstName: user.firstName,
          nickname: user.nickname,
        }
      })
    })
  }

  createComm(comm: AccViewCreateCommEvent, user: IUserProfile) {
    const giftIdx = this.getGiftIdxByGiftId(comm.entityId)
    if (giftIdx === -1) throw new NotFoundException('Подарка не найдено')
    const commIdx = this.getCommIdxByCommId(comm.commId, this.gifts[giftIdx])
    if (commIdx !== -1) throw new NotFoundException('Комментарий существует')
    this.gifts[giftIdx].comms.push({
      ...comm,
      firstName: user.firstName,
      lastName: user.lastName,
      nickname: user.nickname,
    })
  }

  private getGiftIdxByGiftId(giftId: string) {
    return this.gifts.findIndex((g) => g.giftId === giftId)
  }

  private getCommIdxByCommId(commId: string, gift: IGiftView) {
    return gift.comms.findIndex((c) => c.commId === commId)
  }
}
