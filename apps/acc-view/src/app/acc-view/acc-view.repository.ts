import { Injectable, NotFoundException } from '@nestjs/common'
import { IAccView } from '@gift/interfaces'
import { AccView } from './acc-view.model'
import { InjectModel } from '@nestjs/mongoose'
import { Model } from 'mongoose'

@Injectable()
export class AccViewRepository {
  constructor(
    @InjectModel(AccView.name) private readonly accViewModel: Model<AccView>,
  ) {}

  async create(accView: IAccView) {
    const accViewCandidate = await this.accViewModel.findOne({
      'user.userId': accView.user.userId,
    })
    if (accViewCandidate)
      throw new NotFoundException('Пользователь уже существует')
    return this.accViewModel.create(accView)
  }

  async update(accView: IAccView) {
    const accViewCandidate = await this.accViewModel.findOne({
      'user.userId': accView.user.userId,
    })
    if (!accViewCandidate) throw new NotFoundException('Пользователь не найден')
    accViewCandidate.gifts = accView.gifts
    accViewCandidate.user = accView.user
    return accViewCandidate.save()
  }

  async getAccViewByUserId(userId: string) {
    const accView = await this.accViewModel.findOne({ 'user.userId': userId })
    if (!accView) throw new NotFoundException('Пользователь не найден')
    return accView
  }

  getAccViewsByCommUserId(userId: string) {
    return this.accViewModel.find({
      'gifts.comms.userId': userId,
    })
  }
}
