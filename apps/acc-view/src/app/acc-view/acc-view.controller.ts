import { Injectable, UsePipes } from '@nestjs/common'
import {
  RabbitPayload,
  RabbitRPC,
  RabbitSubscribe,
} from '@golevelup/nestjs-rabbitmq'
import {
  AccViewCreateCommEvent,
  accViewCreateCommKey,
  AccViewCreateGiftEvent,
  accViewCreateGiftKey,
  accViewGetAccViewKey,
  AccViewGetAccViewRequest,
  AccViewGetAccViewResponse,
  AccViewRegisterEvent,
  accViewRegisterEventKey,
  AccViewUpdateCommEvent,
  accViewUpdateCommKey,
  AccViewUpdateGiftEvent,
  accViewUpdateGiftKey,
  AccViewUpdateUserProfileEvent,
  accViewUpdateUserProfileEventKey,
  ResponseStatuses,
} from '@gift/contracts'
import { getDefaultPipeValidator, replyErrorHandler } from '@gift/common'
import { config } from 'dotenv'
import { AccViewService } from './acc-view.service'

// without it process.env.AMQP_EXCHANGE is undefined
config()

@Injectable()
export class AccViewController {
  constructor(private readonly accViewService: AccViewService) {}

  @UsePipes(getDefaultPipeValidator())
  @RabbitSubscribe({
    routingKey: accViewRegisterEventKey,
    exchange: process.env.AMQP_EXCHANGE,
    errorHandler: replyErrorHandler,
  })
  async createUser(@RabbitPayload() payloadReq: AccViewRegisterEvent) {
    await this.accViewService.create(payloadReq)
  }

  @UsePipes(getDefaultPipeValidator())
  @RabbitSubscribe({
    routingKey: accViewUpdateUserProfileEventKey,
    exchange: process.env.AMQP_EXCHANGE,
    errorHandler: replyErrorHandler,
  })
  async updateUser(@RabbitPayload() payloadReq: AccViewUpdateUserProfileEvent) {
    await this.accViewService.update(payloadReq)
  }

  @UsePipes(getDefaultPipeValidator())
  @RabbitSubscribe({
    routingKey: accViewCreateGiftKey,
    exchange: process.env.AMQP_EXCHANGE,
    errorHandler: replyErrorHandler,
  })
  async createGift(@RabbitPayload() payloadReq: AccViewCreateGiftEvent) {
    await this.accViewService.createGift(payloadReq)
  }

  @UsePipes(getDefaultPipeValidator())
  @RabbitSubscribe({
    routingKey: accViewUpdateGiftKey,
    exchange: process.env.AMQP_EXCHANGE,
    errorHandler: replyErrorHandler,
  })
  async updateGift(@RabbitPayload() payloadReq: AccViewUpdateGiftEvent) {
    await this.accViewService.updateGift(payloadReq)
  }

  @UsePipes(getDefaultPipeValidator())
  @RabbitSubscribe({
    routingKey: accViewCreateCommKey,
    exchange: process.env.AMQP_EXCHANGE,
    errorHandler: replyErrorHandler,
  })
  async createComm(@RabbitPayload() payloadReq: AccViewCreateCommEvent) {
    await this.accViewService.createComm(payloadReq)
  }

  @UsePipes(getDefaultPipeValidator())
  @RabbitSubscribe({
    routingKey: accViewUpdateCommKey,
    exchange: process.env.AMQP_EXCHANGE,
    errorHandler: replyErrorHandler,
  })
  async updateComm(@RabbitPayload() payloadReq: AccViewUpdateCommEvent) {
    await this.accViewService.updateComm(payloadReq)
  }

  @UsePipes(getDefaultPipeValidator())
  @RabbitRPC({
    routingKey: accViewGetAccViewKey,
    exchange: process.env.AMQP_EXCHANGE,
    errorHandler: replyErrorHandler,
  })
  async getAccView(
    @RabbitPayload() payloadReq: AccViewGetAccViewRequest,
  ): Promise<AccViewGetAccViewResponse> {
    const payload = await this.accViewService.getAccView(payloadReq)
    return {
      payload,
      status: ResponseStatuses.success,
    }
  }
}
