import { ConfigModule } from '@nestjs/config'
import { Module } from '@nestjs/common'
import { MongooseModule } from '@nestjs/mongoose'
import { AccViewModule } from './acc-view/acc-view.module'
import { getMongoConfig } from './configs/mongo.config'

@Module({
  imports: [
    ConfigModule.forRoot({ isGlobal: true }),
    MongooseModule.forRootAsync(getMongoConfig()),
    AccViewModule,
  ],
})
export class AppModule {}
