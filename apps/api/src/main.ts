import { NestFactory } from '@nestjs/core'
import {
  FastifyAdapter,
  NestFastifyApplication,
} from '@nestjs/platform-fastify'
import { AppModule } from './app/app.module'
import cookie from '@fastify/cookie'
import helmet from '@fastify/helmet'
import cors from '@fastify/cors'
import { getDefaultPipeValidator } from '@gift/common'
import { getOrigins } from './origin'

const PORT = process.env.PORT ?? 4002

async function bootstrap() {
  const app = await NestFactory.create<NestFastifyApplication>(
    AppModule,
    new FastifyAdapter(),
  )
  app.useGlobalPipes(getDefaultPipeValidator())
  await app.register(cookie)
  await app.register(helmet, {
    contentSecurityPolicy: false,
  })
  await app.register(cors, {
    origin: getOrigins(),
    credentials: true,
  })

  await app.listen(PORT, '0.0.0.0')
  console.log('api was started')
}

bootstrap()
