import { Controller, Get, HttpException, UseGuards } from '@nestjs/common'
import { AmqpConnection } from '@golevelup/nestjs-rabbitmq'
import { ConfigService } from '@nestjs/config'
import {
  accViewGetAccViewKey,
  AccViewGetAccViewResponse,
} from '@gift/contracts'
import { isError } from '@gift/common'
import { JwtAccessGuard } from '../guards/jwt-access.guard'
import { GetUser } from '../decorator/get-user.decorator'
import { IUserInToken } from '@gift/interfaces'

@Controller('/')
export class AccViewController {
  constructor(
    private readonly amqpConnection: AmqpConnection,
    private readonly configService: ConfigService,
  ) {}

  @UseGuards(JwtAccessGuard)
  @Get()
  async get(@GetUser() user: IUserInToken) {
    const res = await this.amqpConnection.request<AccViewGetAccViewResponse>({
      exchange: this.configService.getOrThrow('AMQP_EXCHANGE'),
      routingKey: accViewGetAccViewKey,
      payload: user,
    })

    if (isError(res)) {
      throw new HttpException(res.error, res.error.statusCode)
    }

    return res.payload
  }
}
