import {
  Body,
  Controller,
  ForbiddenException,
  Get,
  HttpException,
  Patch,
  Post,
  Query,
  UseGuards,
} from '@nestjs/common'
import { AmqpConnection } from '@golevelup/nestjs-rabbitmq'
import { ConfigService } from '@nestjs/config'
import {
  commCreateCommKey,
  CommCreateCommRequest,
  CommCreateCommResponse,
  commGetCommsKey,
  CommGetCommsRequest,
  CommGetCommsResponse,
  commUpdateCommKey,
  CommUpdateCommRequest,
  CommUpdateCommResponse,
} from '@gift/contracts'
import { isError } from '@gift/common'
import { JwtAccessGuard } from '../guards/jwt-access.guard'
import { GetUser } from '../decorator/get-user.decorator'
import { IUserInToken } from '@gift/interfaces'

@Controller('/comms')
export class CommController {
  constructor(
    private readonly amqpConnection: AmqpConnection,
    private readonly configService: ConfigService,
  ) {}

  @UseGuards(JwtAccessGuard)
  @Post()
  async create(
    @Body() payload: CommCreateCommRequest,
    @GetUser() user: IUserInToken,
  ) {
    if (payload.userId !== user.userId) throw new ForbiddenException()
    const res = await this.amqpConnection.request<CommCreateCommResponse>({
      exchange: this.configService.getOrThrow('AMQP_EXCHANGE'),
      routingKey: commCreateCommKey,
      payload,
    })

    if (isError(res)) {
      throw new HttpException(res.error, res.error.statusCode)
    }

    return res.payload
  }

  @UseGuards(JwtAccessGuard)
  @Patch()
  async update(
    @Body() payload: CommUpdateCommRequest,
    @GetUser() user: IUserInToken,
  ) {
    if (payload.userId !== user.userId) throw new ForbiddenException()
    const res = await this.amqpConnection.request<CommUpdateCommResponse>({
      exchange: this.configService.getOrThrow('AMQP_EXCHANGE'),
      routingKey: commUpdateCommKey,
      payload,
    })

    if (isError(res)) {
      throw new HttpException(res.error, res.error.statusCode)
    }

    return res.payload
  }

  @UseGuards(JwtAccessGuard)
  @Get()
  async get(@Query() payload: CommGetCommsRequest) {
    const res = await this.amqpConnection.request<CommGetCommsResponse>({
      exchange: this.configService.getOrThrow('AMQP_EXCHANGE'),
      routingKey: commGetCommsKey,
      payload,
    })

    if (isError(res)) {
      throw new HttpException(res.error, res.error.statusCode)
    }

    return res.payload
  }
}
