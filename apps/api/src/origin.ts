export const getOrigins = () => {
  const ORIGIN = process.env.ORIGIN
  if (!ORIGIN) return '*'
  return ORIGIN.split(',').filter(Boolean)
}
