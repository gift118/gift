import { IGift, PartialBy } from '@gift/interfaces'
import { cloneDeep, escape } from 'lodash'

export class GiftEntity implements IGift {
  giftId: string
  userId: string
  title: string
  text: string

  constructor(gift: IGift) {
    this.giftId = gift.giftId
    this.userId = gift.userId
    this.title = escape(gift.title)
    this.text = escape(gift.text)
  }

  update(gift: PartialBy<IGift, 'giftId' | 'userId'>): GiftEntity {
    const cpGift = cloneDeep(gift)
    delete cpGift.giftId
    delete cpGift.userId
    return new GiftEntity({ ...this, ...cpGift })
  }

  getGiftEvent() {
    return cloneDeep(this)
  }
}
