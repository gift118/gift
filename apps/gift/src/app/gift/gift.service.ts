import { BadRequestException, Injectable } from '@nestjs/common'
import { ICreateGift, IGift } from '@gift/interfaces'
import { GiftRepository } from './gift.repository'
import { GiftEntity } from './gift.entity'
import {
  accViewCreateGiftKey,
  accViewUpdateGiftKey,
  GiftGetGiftsRequest,
} from '@gift/contracts'
import { AmqpConnection } from '@golevelup/nestjs-rabbitmq'
import { ConfigService } from '@nestjs/config'

@Injectable()
export class GiftService {
  constructor(
    private readonly amqpConnection: AmqpConnection,
    private readonly giftRepository: GiftRepository,
    private readonly configService: ConfigService,
  ) {}

  async create(gift: ICreateGift): Promise<IGift> {
    const newGift = await this.giftRepository.create(gift)
    const giftEntity = new GiftEntity(newGift)
    this.amqpConnection.publish(
      this.configService.getOrThrow('AMQP_EXCHANGE'),
      accViewCreateGiftKey,
      giftEntity.getGiftEvent(),
    )
    return giftEntity
  }

  async get(query: GiftGetGiftsRequest): Promise<IGift[]> {
    return this.giftRepository.findMany(query)
  }

  async update(gift: IGift): Promise<IGift> {
    const giftFromDb = await this.giftRepository.findById(gift.giftId)
    if (!giftFromDb) throw new BadRequestException('Gift not found')
    const giftEntity = new GiftEntity(giftFromDb).update(gift)
    this.amqpConnection.publish(
      this.configService.getOrThrow('AMQP_EXCHANGE'),
      accViewUpdateGiftKey,
      giftEntity.getGiftEvent(),
    )
    return this.giftRepository.update(giftEntity)
  }
}
