import { Module } from '@nestjs/common'
import { GiftController } from './gift.controller'
import { GiftService } from './gift.service'
import { PrismaModule } from '../prisma/prisma.module'
import { GiftRepository } from './gift.repository'
import { RabbitMQModule } from '@golevelup/nestjs-rabbitmq'
import { getRMQConfig } from '../configs/rmq.config'

@Module({
  imports: [
    PrismaModule,
    RabbitMQModule.forRootAsync(RabbitMQModule, getRMQConfig()),
  ],
  providers: [GiftController, GiftService, GiftRepository],
})
export class GiftModule {}
