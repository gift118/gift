import { Module } from '@nestjs/common'
import { ConfigModule } from '@nestjs/config'
import { GiftModule } from './gift/gift.module'
import { PrismaModule } from './prisma/prisma.module'

@Module({
  imports: [ConfigModule.forRoot({ isGlobal: true }), GiftModule, PrismaModule],
})
export class AppModule {}
