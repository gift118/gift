import { IUserProfile } from '@gift/interfaces';
export declare const accViewRegisterEventKey = "acc-view.register.event";
export declare class AccViewRegisterEvent implements IUserProfile {
    userId: string;
    email: string;
    isActive: boolean;
    firstName: string | null;
    lastName: string | null;
    nickname: string | null;
    bio: string | null;
}
