import { IGift } from '@gift/interfaces';
export declare const accViewUpdateGiftKey = "acc-view.update-gift.event";
export declare class AccViewUpdateGiftEvent implements IGift {
    giftId: string;
    userId: string;
    title: string;
    text: string;
}
