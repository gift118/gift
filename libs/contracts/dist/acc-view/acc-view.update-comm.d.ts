import { EntityType, IComm } from '@gift/interfaces';
export declare const accViewUpdateCommKey = "acc-view.update-comm.event";
export declare class AccViewUpdateCommEvent implements IComm {
    commId: string;
    entity: EntityType;
    entityId: string;
    userId: string;
    text: string;
}
