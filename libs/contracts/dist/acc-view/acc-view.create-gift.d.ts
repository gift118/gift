import { IGift } from '@gift/interfaces';
export declare const accViewCreateGiftKey = "acc-view.create-gift.event";
export declare class AccViewCreateGiftEvent implements IGift {
    giftId: string;
    userId: string;
    title: string;
    text: string;
}
