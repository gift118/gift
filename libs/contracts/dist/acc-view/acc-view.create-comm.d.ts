import { EntityType, IComm } from '@gift/interfaces';
export declare const accViewCreateCommKey = "acc-view.create-comm.event";
export declare class AccViewCreateCommEvent implements IComm {
    commId: string;
    entity: EntityType;
    entityId: string;
    userId: string;
    text: string;
}
