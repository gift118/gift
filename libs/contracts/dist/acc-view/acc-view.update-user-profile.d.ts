import { IUserProfile } from '@gift/interfaces';
export declare const accViewUpdateUserProfileEventKey = "account.update-user-profile.event";
export declare class AccViewUpdateUserProfileEvent implements IUserProfile {
    userId: string;
    email: string;
    isActive: boolean;
    firstName: string | null;
    lastName: string | null;
    nickname: string | null;
    bio: string | null;
}
