import { ResponseError } from '../common/response/response-error';
import { ResponseSuccess } from '../common/response/response-success';
import { IAccView } from '@gift/interfaces';
export declare const accViewGetAccViewKey = "acc-view.get-acc-view.query";
export declare class AccViewGetAccViewRequest {
    userId: string;
}
export declare class AccViewGetAccViewResponseSuccess extends ResponseSuccess {
    payload: IAccView;
}
export declare type AccViewGetAccViewResponse = AccViewGetAccViewResponseSuccess | ResponseError;
