import { EntityType, IComm, ICreateComm } from '@gift/interfaces';
import { ResponseError } from '../common/response/response-error';
import { ResponseSuccess } from '../common/response/response-success';
export declare const commCreateCommKey = "comm.create-comm.command";
export declare class CommCreateCommRequest implements ICreateComm {
    entity: EntityType;
    entityId: string;
    userId: string;
    text: string;
}
declare class CommCreateCommRequestSuccess extends ResponseSuccess {
    payload: IComm;
}
export declare type CommCreateCommResponse = CommCreateCommRequestSuccess | ResponseError;
export {};
