import { EntityType, IComm } from '@gift/interfaces';
import { ResponseError } from '../common/response/response-error';
import { ResponseSuccess } from '../common/response/response-success';
export declare const commUpdateCommKey = "comm.update-comm.command";
export declare class CommUpdateCommRequest implements IComm {
    commId: string;
    entity: EntityType;
    entityId: string;
    giftId: string;
    userId: string;
    text: string;
}
declare class CommUpdateCommResponseSuccess extends ResponseSuccess {
    payload: IComm;
}
export declare type CommUpdateCommResponse = CommUpdateCommResponseSuccess | ResponseError;
export {};
