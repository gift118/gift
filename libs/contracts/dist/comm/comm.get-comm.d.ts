import { IComm, IGetCommBy, IPagindation } from '@gift/interfaces';
import { ResponseError } from '../common/response/response-error';
import { ResponseSuccess } from '../common/response/response-success';
export declare const commGetCommsKey = "comm.get-comms.command";
export declare class CommGetCommsRequest implements IPagindation, IGetCommBy {
    commId?: string;
    userId?: string;
    entityId?: string;
    skip?: number;
    take?: number;
}
declare class CommGetCommsResponseSuccess extends ResponseSuccess {
    payload: IComm[];
}
export declare type CommGetCommsResponse = CommGetCommsResponseSuccess | ResponseError;
export {};
