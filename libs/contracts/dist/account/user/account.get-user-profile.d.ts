import { ResponseError } from '../../common/response/response-error';
import { ResponseSuccess } from '../../common/response/response-success';
import { IUserProfile } from '@gift/interfaces';
export declare const accountGetUserProfileKey = "account.get-user-profile.query";
export declare class AccountGetUserProfileRequest {
    userId: string;
}
export declare class AccountGetUserProfileResponseSuccess extends ResponseSuccess {
    payload: IUserProfile;
}
export declare type AccountGetUserProfileResponse = AccountGetUserProfileResponseSuccess | ResponseError;
