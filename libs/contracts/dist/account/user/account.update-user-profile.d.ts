import { ResponseError } from '../../common/response/response-error';
import { ResponseSuccess } from '../../common/response/response-success';
import { IUserProfile } from '@gift/interfaces';
export declare const accountUpdateUserProfileKey = "account.update-user-profile.command";
export declare class AccountUpdateUserProfileRequest implements IUserProfile {
    userId: string;
    email: string;
    isActive: boolean;
    firstName: string | null;
    lastName: string | null;
    nickname: string | null;
    bio: string | null;
}
declare class AccountUpdateUserProfileResponseSuccess extends ResponseSuccess {
    payload: IUserProfile;
}
export declare type AccountUpdateUserProfileResponse = AccountUpdateUserProfileResponseSuccess | ResponseError;
export {};
