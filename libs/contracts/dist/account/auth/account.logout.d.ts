import { ResponseError } from '../../common/response/response-error';
import { ResponseSuccess } from '../../common/response/response-success';
import { ILogout } from '@gift/interfaces';
export declare const accountLogoutKey = "account.logout.command";
export declare class AccountLogoutRequest {
    refreshToken: string;
}
declare class AccountLogoutResponseSuccess extends ResponseSuccess {
    payload: ILogout;
}
export declare type AccountLogoutResponse = AccountLogoutResponseSuccess | ResponseError;
export {};
