import { ResponseError } from '../../common/response/response-error';
import { ResponseSuccess } from '../../common/response/response-success';
import { ILogout } from '@gift/interfaces';
export declare const accountLogoutAllKey = "account.logouta-all.command";
export declare class AccountLogoutAllRequest {
    userId: string;
}
declare class AccountLogoutAllResponseSuccess extends ResponseSuccess {
    payload: ILogout;
}
export declare type AccountLogoutAllResponse = AccountLogoutAllResponseSuccess | ResponseError;
export {};
