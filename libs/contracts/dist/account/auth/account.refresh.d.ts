import { ITokens, IUserInToken } from '@gift/interfaces';
import { ResponseError } from '../../common/response/response-error';
import { ResponseSuccess } from '../../common/response/response-success';
export declare const accountRefreshKey = "account.refresh.command";
export declare class UserInTokenDot implements IUserInToken {
    userId: string;
}
export declare class AccountRefreshRequest {
    refreshToken: string;
    user: IUserInToken;
}
declare class AccountRefreshResponseSuccess extends ResponseSuccess {
    payload: ITokens;
}
export declare type AccountRefreshResponse = AccountRefreshResponseSuccess | ResponseError;
export {};
