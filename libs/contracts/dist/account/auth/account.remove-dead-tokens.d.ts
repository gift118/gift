import { ResponseSuccess } from '../../common/response/response-success';
import { ResponseError } from '../../common/response/response-error';
import { IsOk } from '@gift/interfaces';
export declare const accountRemoveDeadTokensKey = "account.remove-dead-tokens.command";
export declare class AccountRemoveDeadTokensRequest {
}
declare class AccountRemoveDeadTokensResponseSuccess extends ResponseSuccess {
    payload: IsOk;
}
export declare type AccountRemoveDeadTokensResponse = AccountRemoveDeadTokensResponseSuccess | ResponseError;
export {};
