import { ITokens } from '@gift/interfaces';
import { ResponseSuccess } from '../../common/response/response-success';
import { ResponseError } from '../../common/response/response-error';
export declare const accountRegisterKey = "account.register.command";
export declare class AccountRegisterRequest {
    email: string;
    password: string;
}
declare class AccountRegisterResponseSuccess extends ResponseSuccess {
    payload: ITokens;
}
export declare type AccountRegisterResponse = AccountRegisterResponseSuccess | ResponseError;
export {};
