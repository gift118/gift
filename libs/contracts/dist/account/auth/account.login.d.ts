import { ITokens } from '@gift/interfaces';
import { ResponseError } from '../../common/response/response-error';
import { ResponseSuccess } from '../../common/response/response-success';
export declare const accountLoginKey = "account.login.command";
export declare class AccountLoginRequest {
    email: string;
    password: string;
}
declare class AccountLoginResponseSuccess extends ResponseSuccess {
    payload: ITokens;
}
export declare type AccountLoginResponse = AccountLoginResponseSuccess | ResponseError;
export {};
