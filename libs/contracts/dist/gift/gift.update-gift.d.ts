import { IGift } from '@gift/interfaces';
import { ResponseError } from '../common/response/response-error';
import { ResponseSuccess } from '../common/response/response-success';
export declare const giftUpdateGiftKey = "gift.update-gift.command";
export declare class GiftUpdateGiftRequest implements IGift {
    giftId: string;
    userId: string;
    title: string;
    text: string;
}
declare class GiftUpdateGiftResponseSuccess extends ResponseSuccess {
    payload: IGift;
}
export declare type GiftUpdateGiftResponse = GiftUpdateGiftResponseSuccess | ResponseError;
export {};
