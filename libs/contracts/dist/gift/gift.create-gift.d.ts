import { ICreateGift, IGift } from '@gift/interfaces';
import { ResponseError } from '../common/response/response-error';
import { ResponseSuccess } from '../common/response/response-success';
export declare const giftCreateGiftKey = "gift.create-gift.command";
export declare class GiftCreateGiftRequest implements ICreateGift {
    userId: string;
    title: string;
    text: string;
}
declare class GiftCreateGiftResponseSuccess extends ResponseSuccess {
    payload: IGift;
}
export declare type GiftCreateGiftResponse = GiftCreateGiftResponseSuccess | ResponseError;
export {};
