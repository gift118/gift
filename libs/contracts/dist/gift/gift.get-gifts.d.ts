import { IGetGiftBy, IGift, IPagindation } from '@gift/interfaces';
import { ResponseError } from '../common/response/response-error';
import { ResponseSuccess } from '../common/response/response-success';
export declare const giftGetGiftsKey = "gift.get-gifts.command";
export declare class GiftGetGiftsRequest implements IPagindation, IGetGiftBy {
    giftId?: string;
    userId?: string;
    skip?: number;
    take?: number;
}
declare class GiftGetGiftsResponseSuccess extends ResponseSuccess {
    payload: IGift[];
}
export declare type GiftGetGiftsResponse = GiftGetGiftsResponseSuccess | ResponseError;
export {};
