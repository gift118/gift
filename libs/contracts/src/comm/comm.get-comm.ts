import { IComm, IGetCommBy, IPagindation } from '@gift/interfaces'
import { IsNumber, IsOptional, IsUUID } from 'class-validator'
import { ResponseError } from '../common/response/response-error'
import { ResponseSuccess } from '../common/response/response-success'
import { Type } from 'class-transformer'

export const commGetCommsKey = 'comm.get-comms.command'

export class CommGetCommsRequest implements IPagindation, IGetCommBy {
  @IsOptional()
  @IsUUID()
  commId?: string

  @IsOptional()
  @IsUUID()
  userId?: string

  @IsOptional()
  @IsUUID()
  entityId?: string

  @Type(() => Number)
  @IsOptional()
  @IsNumber()
  skip?: number

  @Type(() => Number)
  @IsOptional()
  @IsNumber()
  take?: number
}

class CommGetCommsResponseSuccess extends ResponseSuccess {
  payload: IComm[]
}

export type CommGetCommsResponse = CommGetCommsResponseSuccess | ResponseError
