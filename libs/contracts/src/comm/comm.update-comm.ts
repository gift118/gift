import { EntityType, IComm } from '@gift/interfaces'
import { IsString, IsUUID, MaxLength } from 'class-validator'
import { ResponseError } from '../common/response/response-error'
import { ResponseSuccess } from '../common/response/response-success'

export const commUpdateCommKey = 'comm.update-comm.command'

export class CommUpdateCommRequest implements IComm {
  @IsUUID()
  commId: string

  @IsString()
  entity: EntityType = 'GIFT'

  @IsUUID()
  entityId: string

  @IsUUID()
  giftId: string

  @IsUUID()
  userId: string

  @IsString()
  @MaxLength(64)
  text: string
}

class CommUpdateCommResponseSuccess extends ResponseSuccess {
  payload: IComm
}

export type CommUpdateCommResponse =
  | CommUpdateCommResponseSuccess
  | ResponseError
