import { EntityType, IComm, ICreateComm } from '@gift/interfaces'
import { IsString, IsUUID, MaxLength } from 'class-validator'
import { ResponseError } from '../common/response/response-error'
import { ResponseSuccess } from '../common/response/response-success'

export const commCreateCommKey = 'comm.create-comm.command'

export class CommCreateCommRequest implements ICreateComm {
  @IsString()
  entity: EntityType = 'GIFT'

  @IsUUID()
  entityId: string

  @IsUUID()
  userId: string

  @IsString()
  @MaxLength(64)
  text: string
}

class CommCreateCommRequestSuccess extends ResponseSuccess {
  payload: IComm
}

export type CommCreateCommResponse =
  | CommCreateCommRequestSuccess
  | ResponseError
