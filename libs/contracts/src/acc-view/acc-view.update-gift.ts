import { IGift } from '@gift/interfaces'
import { IsString, IsUUID, MaxLength } from 'class-validator'

export const accViewUpdateGiftKey = 'acc-view.update-gift.event'

export class AccViewUpdateGiftEvent implements IGift {
  @IsUUID()
  giftId: string

  @IsUUID()
  userId: string

  @IsString()
  @MaxLength(12)
  title: string

  @IsString()
  @MaxLength(280)
  text: string
}
