import { IsUUID } from 'class-validator'
import { ResponseError } from '../common/response/response-error'
import { ResponseSuccess } from '../common/response/response-success'
import { IAccView } from '@gift/interfaces'

export const accViewGetAccViewKey = 'acc-view.get-acc-view.query'

export class AccViewGetAccViewRequest {
  @IsUUID()
  userId: string
}

export class AccViewGetAccViewResponseSuccess extends ResponseSuccess {
  payload: IAccView
}

export type AccViewGetAccViewResponse =
  | AccViewGetAccViewResponseSuccess
  | ResponseError
