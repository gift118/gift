import { EntityType, IComm } from '@gift/interfaces'
import { IsString, IsUUID, MaxLength } from 'class-validator'

export const accViewCreateCommKey = 'acc-view.create-comm.event'

export class AccViewCreateCommEvent implements IComm {
  @IsUUID()
  commId: string

  @IsString()
  entity: EntityType = 'GIFT'

  @IsUUID()
  entityId: string

  @IsUUID()
  userId: string

  @IsString()
  @MaxLength(64)
  text: string
}
