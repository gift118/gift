import {
  IsBoolean,
  IsEmail,
  IsOptional,
  IsString,
  IsUUID,
} from 'class-validator'
import { IUserProfile } from '@gift/interfaces'

export const accViewRegisterEventKey = 'acc-view.register.event'

export class AccViewRegisterEvent implements IUserProfile {
  @IsUUID()
  userId: string

  @IsEmail()
  email: string

  @IsBoolean()
  isActive: boolean

  @IsOptional()
  @IsString()
  firstName: string | null

  @IsOptional()
  @IsString()
  lastName: string | null

  @IsOptional()
  @IsString()
  nickname: string | null

  @IsOptional()
  @IsString()
  bio: string | null
}
