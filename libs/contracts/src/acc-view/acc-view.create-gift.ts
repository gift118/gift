import { IGift } from '@gift/interfaces'
import { IsString, IsUUID, MaxLength } from 'class-validator'

export const accViewCreateGiftKey = 'acc-view.create-gift.event'

export class AccViewCreateGiftEvent implements IGift {
  @IsUUID()
  giftId: string

  @IsUUID()
  userId: string

  @IsString()
  @MaxLength(12)
  title: string

  @IsString()
  @MaxLength(280)
  text: string
}
