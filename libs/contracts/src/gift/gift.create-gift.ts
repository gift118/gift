import { ICreateGift, IGift } from '@gift/interfaces'
import { IsString, IsUUID, MaxLength } from 'class-validator'
import { ResponseError } from '../common/response/response-error'
import { ResponseSuccess } from '../common/response/response-success'

export const giftCreateGiftKey = 'gift.create-gift.command'

export class GiftCreateGiftRequest implements ICreateGift {
  @IsUUID()
  userId: string

  @IsString()
  @MaxLength(12)
  title: string

  @IsString()
  @MaxLength(280)
  text: string
}

class GiftCreateGiftResponseSuccess extends ResponseSuccess {
  payload: IGift
}

export type GiftCreateGiftResponse =
  | GiftCreateGiftResponseSuccess
  | ResponseError
