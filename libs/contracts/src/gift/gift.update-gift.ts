import { IGift } from '@gift/interfaces'
import { IsString, IsUUID, MaxLength } from 'class-validator'
import { ResponseError } from '../common/response/response-error'
import { ResponseSuccess } from '../common/response/response-success'

export const giftUpdateGiftKey = 'gift.update-gift.command'

export class GiftUpdateGiftRequest implements IGift {
  @IsUUID()
  giftId: string

  @IsUUID()
  userId: string

  @IsString()
  @MaxLength(12)
  title: string

  @IsString()
  @MaxLength(280)
  text: string
}

class GiftUpdateGiftResponseSuccess extends ResponseSuccess {
  payload: IGift
}

export type GiftUpdateGiftResponse =
  | GiftUpdateGiftResponseSuccess
  | ResponseError
