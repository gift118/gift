import { IsJWT, IsNotEmptyObject, IsUUID } from 'class-validator'
import { ITokens, IUserInToken } from '@gift/interfaces'
import { ResponseError } from '../../common/response/response-error'
import { ResponseSuccess } from '../../common/response/response-success'

export const accountRefreshKey = 'account.refresh.command'

export class UserInTokenDot implements IUserInToken {
  @IsUUID()
  userId: string
}

export class AccountRefreshRequest {
  @IsJWT()
  refreshToken: string

  @IsNotEmptyObject()
  user: IUserInToken
}

class AccountRefreshResponseSuccess extends ResponseSuccess {
  payload: ITokens
}
export type AccountRefreshResponse =
  | AccountRefreshResponseSuccess
  | ResponseError
