import { ITokens } from '@gift/interfaces'
import { IsEmail, IsString, MaxLength, MinLength } from 'class-validator'
import { ResponseError } from '../../common/response/response-error'
import { ResponseSuccess } from '../../common/response/response-success'

export const accountLoginKey = 'account.login.command'

export class AccountLoginRequest {
  @IsEmail()
  @MaxLength(320)
  email: string

  @IsString()
  @MinLength(8)
  @MaxLength(32)
  password: string
}

class AccountLoginResponseSuccess extends ResponseSuccess {
  payload: ITokens
}

export type AccountLoginResponse = AccountLoginResponseSuccess | ResponseError
