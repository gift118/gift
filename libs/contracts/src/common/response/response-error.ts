import { ResponseStatuses } from './response-statuses'

export interface IApiError {
  statusCode: number
  message: string | string[]
  error: string
}

export class ResponseError {
  status = ResponseStatuses.error
  error: IApiError
}
