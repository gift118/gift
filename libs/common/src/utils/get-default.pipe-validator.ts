import { ValidationPipe } from '@nestjs/common'

export const getDefaultPipeValidator = () =>
  new ValidationPipe({
    whitelist: true,
    forbidNonWhitelisted: true,
    transform: true,
    validateCustomDecorators: true,
  })
