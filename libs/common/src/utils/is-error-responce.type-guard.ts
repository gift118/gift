import {
  ResponseError,
  ResponseStatuses,
  ResponseSuccess,
} from '@gift/contracts'

export const isError = (
  res: ResponseError | ResponseSuccess,
): res is ResponseError => {
  return res.status === ResponseStatuses.error
}
