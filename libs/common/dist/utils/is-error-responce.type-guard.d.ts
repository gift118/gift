import { ResponseError, ResponseSuccess } from '@gift/contracts';
export declare const isError: (res: ResponseError | ResponseSuccess) => res is ResponseError;
