import amqplib from 'amqplib';
import { HttpException } from '@nestjs/common';
export declare function replyErrorHandler(channel: amqplib.Channel, msg: amqplib.ConsumeMessage, error: HttpException): void;
