import { IGift } from './gift.interface';
export declare type ICreateGift = Omit<IGift, 'giftId'>;
