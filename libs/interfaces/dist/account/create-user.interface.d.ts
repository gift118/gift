import { IUser } from './user.interface';
export declare type ICreateUser = Pick<IUser, 'email' | 'passwordHash'>;
