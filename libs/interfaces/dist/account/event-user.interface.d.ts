import { IUser } from './user.interface';
export declare type IEventUser = Omit<IUser, 'passwordHash'>;
