import { ISession } from './session.interface';
export declare type ICreateSession = Pick<ISession, 'accessToken' | 'refreshToken' | 'userId'>;
