import { IUser } from './user.interface';
export declare type IUserProfile = Omit<IUser, 'passwordHash'>;
