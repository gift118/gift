import { IUser } from '../account/user.interface';
import { IGift } from '../gift/gift.interface';
import { IComm } from '../comm/comm.interface';
export declare type IUserView = Pick<IUser, 'userId' | 'lastName' | 'firstName' | 'bio' | 'email' | 'nickname'>;
export declare type ICommView = Pick<IComm, 'commId' | 'userId' | 'text'> & Pick<IUser, 'lastName' | 'firstName' | 'nickname'>;
export interface IGiftView extends Pick<IGift, 'giftId' | 'text' | 'title'> {
    comms: ICommView[];
}
export interface IAccView {
    user: IUserView;
    gifts: IGiftView[];
}
