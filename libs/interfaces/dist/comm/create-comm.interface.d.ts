import { IComm } from './comm.interface';
export declare type ICreateComm = Omit<IComm, 'commId'>;
