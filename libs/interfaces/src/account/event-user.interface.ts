import { IUser } from './user.interface'

export type IEventUser = Omit<IUser, 'passwordHash'>
