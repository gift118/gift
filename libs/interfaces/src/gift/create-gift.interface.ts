import { IGift } from './gift.interface'

export type ICreateGift = Omit<IGift, 'giftId'>
