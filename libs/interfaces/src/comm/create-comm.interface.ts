import { IComm } from './comm.interface'

export type ICreateComm = Omit<IComm, 'commId'>
