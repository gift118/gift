import { EntityType } from '../common/entity.type'

export interface IComm {
  commId: string
  userId: string
  entity: EntityType
  entityId: string
  text: string
}
